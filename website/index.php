<?php 
require_once 'database/dbFunctions.php';

$data = getDbData();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>php-mysql</title>
</head>
<body>

    <?php 
    // echo '<pre>';
    // print_r($data);
    // echo '</pre>';
    

    while($singleData = $data->fetch_assoc()){
        echo "ID: ". $singleData['id'] ."<br>";
        echo "First Name: ". $singleData['first_name'] ."<br>";
        echo "Last Name: ". $singleData['last_name'] ."<br>";
        echo "Designation: ". $singleData['designation'] ."<br><br><br>";
    }
    ?>
    
</body>
</html>